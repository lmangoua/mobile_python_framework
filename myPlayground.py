from Elements import AndroidElements
from appium.webdriver.common.touch_action import TouchAction
from AppiumSetup import AppiumSetup
import subprocess

AppLoad = AndroidElements.AppLoad
Login = AndroidElements.Login
Home = AndroidElements.HomeScreen
Beneficiary = AndroidElements.Beneficiary
PaymentReason = AndroidElements.PaymentReason
AdditionalInfo = AndroidElements.AdditionalInfo
Account = AndroidElements.Account
Declaration = AndroidElements.Declaration

driver = AppiumSetup.driver

#Appload
driver.find_element_by_id(AppLoad.ConfirmButton).click()
driver.implicitly_wait(10)
driver.find_element_by_id(AppLoad.AndroidAllow).click()
driver.implicitly_wait(20)
driver.find_element_by_id(AppLoad.GotIt).click()

#login
driver.find_element_by_id(Login.CountrySelect).click()
editText = driver.find_element_by_class_name(Login.editText)
editText.send_keys("South Africa")

driver.find_element_by_id(Login.SelectedCountry).click()
driver.find_element_by_id(Login.btnContinue).click()
driver.implicitly_wait(20)

editUsername = driver.find_element_by_id(Login.email)
driver.implicitly_wait(20)
editUsername.clear()
editUsername.send_keys("novoretail1@novo.com")

driver.find_element_by_id(Login.Password).click()
subprocess.call("adb shell input text " +str(Login.SA_Password), shell=True)
driver.hide_keyboard()

driver.find_element_by_id(Login.btnLogin).click()
driver.find_element_by_id(Login.btnGotIt_Security).click()

#Homescreen
sourceAmount = driver.find_element_by_id(Home.sourceCurrency)
sourceAmount.send_keys(Home.trxAmt)
driver.find_element_by_id(Login.btnSelectBeneficiary).click()

#TouchAction(driver)   .press(x=613, y=1290)   .move_to(x=626, y=1080)   .release()   .perform()

#The below is used when the device screensize is small, therefore when the Select beneficiary button is not visible on screen

#actions2 = TouchAction(driver)
#actions2.press(x=651, y=1200).move_to(x=643, y=900).release().perform()
#driver.implicitly_wait(20)
#driver.find_element_by_id(Home.btnSelectBeneficiary).click()

#OR

#subprocess.call("adb shell input touchscreen tap "+str(ll_search_location.get('x')) +" " + str(ll_search_location.get('y')), shell=True)

#Beneficiary
# SearchBeneficiary = driver.find_element_by_id(Beneficiary.Search)
# SearchBeneficiary.send_keys(Beneficiary.Name)
driver.find_element_by_id(Beneficiary.selectedBeneficiary).click()
driver.find_element_by_id(Beneficiary.btnNext).click()

#Payment Reason
driver.find_element_by_xpath(PaymentReason.Gift).click()
driver.find_element_by_id(PaymentReason.btnNext).click()

#SDA and Acknowledgement step
driver.find_element_by_xpath(AdditionalInfo.SDA_Acknowledgement).click()
driver.find_element_by_xpath(AdditionalInfo.GiftToSelf).click()
driver.find_element_by_id(AdditionalInfo.btnNext).click()

#Select account to transact on
driver.find_element_by_id(Account.accountRadioBtn).click()
# to add logic to confirm transacting amount and the fees : AmountLbl = 'za.co.absa.novofx.beta:id/tv_updating_exchange_rate'
driver.find_element_by_id(Account.btnNext).click()


#Accept/Decline Declaration
driver.find_element_by_id(Declaration.Accept).click() #Accept Declaration
driver.find_element_by_id(Declaration.btnDeclarationContinue).click()

# PS: From SDA section the app timed out a few times therefore selecting the account is not always reached. Working on a workaround on adb to find all elements once off and action on them



driver.quit()

