# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

# UI Automation Setup (Mac OS and Windows)

## Requirements: 

•	Pycharm
•	Android Studio (With AVP manager. Built in)
•	XCode
•	NodeJS and npm
•	Appium Desktop & Appium Client (Latest)
•	Python (Latest)
•	Git
•	Selenium Web Driver (Included under python install)
•	VSTS access to projects 

# Application and library installation: 

•	Install [Pycharm](https://www.jetbrains.com/pycharm/download/) 
The community edition is sufficient. Further setup will be gone through when automation project is being run.  

•	Install Android Studio (https://developer.android.com/studio)
Android studio wil be used to run the Android project to create the APKs which will be tested against.

•	Install XCode. (Available from the App Store on Mac) 
You may be required to create an Apple Account to be able to install applications from the App store. Not to be confused with the system account .

•	Install NodeJS and npm(https://nodejs.org/en/download/)
Install NodeJS and confirm npm installation is successful by opening the terminal and checking the version.  npm --version.
Installation is successful by having a version number returned.

•	Install latest version of appium
Install Appium Client via the terminal. Npm install -g appium@1.12.1 
Confirm via: appium -v 

•	Install Python (https://www.python.org/downloads/)
Once python has been install, install the required packages. 
Requests library : pip3 install requests
YAML: pip3 install yaml
Selenium: pip3 install selenium
Pytest: pip3 install pytest

•	Install Git (https://desktop.github.com)
Git should be installed automatically with the XCode install. If not, you can use the above link to install the desktop gui application which will include the command line tools. 
Confirm git installation via terminal : git –version

# Setting up the automation project (Quick start)

1.	Create a folder on PC of where you would like to have the project cloned to. 
Load the terminal and cd to the created folder and execute the following command
 

Cloning from Bitbucket: 
https://bitbucket.org/lmangoua/mobile_python_framework


2.	Load the project in Pycharm
You should have an environment as below: 

![image.png](https://res.cloudinary.com/bobystore/image/upload/v1626865969/Screenshots/1.png)


# Android:
For Windows and Mac machines, you can run the project on any machine without any change to configuration. 
You will however have to run the automation on a physical device. For this you have to update the AppiumSetup.py file to include your devices capabilities. 

To get your device udid, type the following ; `avd devices`

Update AppiumSetup with your device details you obtained from avd (udid). Update the driver details as well (last highlight in the below screenshot)
for iOS, please run `instruments -s devices` and get the device id which you will update in the udid field

![image.png](https://res.cloudinary.com/bobystore/image/upload/v1626865969/Screenshots/2.png)

 
To view hidden files and folders, do the following commands on keyboard. (cmd + shift + . )

# Running the project:

Go to test suites and open iPhoneSE_RSA.py.
Right click within the opened file and select `Run 'iPhoneSE_RSA'` . This will automatically run the automation test inside the simulator.

#Determine which tests are to be run:
In Test_Suites folder is where you can place your test scenarios. Do keep in mind that there is a Test folder of which you can load the all the screens and touchpoints in the application. The Test_Suites section is where you can specify what exactly you would like to test. 

![image.png](https://res.cloudinary.com/bobystore/image/upload/v1626865969/Screenshots/3.png)

