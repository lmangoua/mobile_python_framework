from Elements import AndroidElements
from appium.webdriver.common.touch_action import TouchAction
from AppiumSetup import AppiumSetup
import subprocess

AppLoad = AndroidElements.AppLoad
Login = AndroidElements.Login
Home = AndroidElements.HomeScreen
Beneficiary = AndroidElements.Beneficiary
PaymentReason = AndroidElements.PaymentReason
AdditionalInfo = AndroidElements.AdditionalInfo
Account = AndroidElements.Account
Declaration = AndroidElements.Declaration

driver = AppiumSetup.driver

#Appload
def test_LoadApp():
    driver.find_element_by_id(AppLoad.ConfirmButton).click()
    driver.implicitly_wait(10)
    driver.find_element_by_id(AppLoad.AndroidAllow).click()
    driver.implicitly_wait(20)
    driver.find_element_by_id(AppLoad.GotIt).click()

#login
def test_Login():
    driver.find_element_by_id(Login.CountrySelect).click()
    editText = driver.find_element_by_class_name(Login.editText)
    editText.send_keys("South Africa")

    driver.find_element_by_id(Login.SelectedCountry).click()
    driver.find_element_by_id(Login.btnContinue).click()
    driver.implicitly_wait(20)

    editUsername = driver.find_element_by_id(Login.email)
    driver.implicitly_wait(20)
    editUsername.clear()
    editUsername.send_keys("novoretail1@novo.com")

    driver.find_element_by_id(Login.Password).click()
    subprocess.call("adb shell input text " +str(Login.SA_Password), shell=True)
    driver.hide_keyboard()

    driver.find_element_by_id(Login.btnLogin).click()
    driver.find_element_by_id(Login.btnGotIt_Security).click()

#Homescreen
def test_HomeScreen():
    sourceAmount = driver.find_element_by_id(Home.sourceCurrency)
    sourceAmount.send_keys(Home.trxAmt)
    driver.find_element_by_id(Login.btnSelectBeneficiary).click()

#TouchAction(driver)   .press(x=613, y=1290)   .move_to(x=626, y=1080)   .release()   .perform()

#from appium.webdriver.common.touch_action import TouchAction
# ...
# actions = TouchAction(driver)
# #actions.scroll_from_element(element, 10, 100)
# actions.scroll(10, 200)
# actions.perform()

#actions2 = TouchAction(driver)
#actions2.press(x=651, y=1200).move_to(x=643, y=900).release().perform()
#driver.implicitly_wait(20)
#driver.find_element_by_id(Home.btnSelectBeneficiary).click()

#Beneficiary
def test_Beneficiary():
   # SearchBeneficiary = driver.find_element_by_id(Beneficiary.Search)
   # SearchBeneficiary.send_keys(Beneficiary.Name)
    driver.find_element_by_id(Beneficiary.selectedBeneficiary).click()
    driver.find_element_by_id(Beneficiary.btnNext).click()
def test_Exchange():
    #Payment Reason
    driver.find_element_by_xpath(PaymentReason.Gift).click()
    driver.find_element_by_id(PaymentReason.btnNext).click()

def test_SDA():
driver.find_element_by_xpath(AdditionalInfo.SDA_Acknowledgement).click()
driver.find_element_by_xpath(AdditionalInfo.GiftToSelf).click()
driver.find_element_by_id(AdditionalInfo.btnNext).click()




# CountriesList = driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView')
# for value in CountriesList:
#     countryName = value.get_attribute("text")
#     print(countryName)

    # countrieslist = driver.find_element_by_xpath('/hierarchy/android.widget.framelayout/android.widget.linearlayout/android.widget.framelayout/android.widget.linearlayout/android.widget.framelayout/android.widget.linearlayout/android.support.v7.widget.recyclerview/android.widget.linearlayout[2]/android.widget.framelayout/android.widget.linearlayout/android.widget.linearlayout/android.widget.textview[1]')
    # for value in countrieslist:
    #     countryname = value.getattribute("text")
    #     print(countryname)

#Search for country
#search_country = driver.find_element_by_id('za.co.absa.novofx.beta:id/tv_search')
#search_country.click()/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView

#search_country.send_keys('South Africa')

#ll_search_location = driver.find_element_by_id('za.co.absa.novofx.beta:id/ll_search').location

#ll_search_location = driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.EditText').location


#driver.find_element_by_id('za.co.absa.novofx.beta:id/tv_search').send_keys()

#search_country = driver.find_element_by_id('za.co.absa.novofx.beta:id/tv_search')
#driver.set_value(search_country, 'South Africa')
#subprocess.call("adb shell input touchscreen tap "+str(ll_search_location.get('x')) +" " + str(ll_search_location.get('y')), shell=True)

#subprocess.call("adb shell input text South%sAfrica", shell=True)

#driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.EditText')



#Select South African flag thats available on screen   za.co.absa.novofx.beta:id/iv_country_flag
#driver.find_element_by_id('za.co.absa.novofx.beta:id/iv_country_flag').click()


driver.quit()

