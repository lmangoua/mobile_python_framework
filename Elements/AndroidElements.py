

class AppLoad:
    # adb shell pm clear my.app.package.name
    # click Proceed button
    ConfirmButton = 'za.co.absa.novofx.beta:id/bt_dialog_general_confirm'
    AndroidAllow = 'com.android.packageinstaller:id/permission_allow_button'
    GotIt = 'za.co.absa.novofx.beta:id/bt_got_it'


class Login:
    CountrySelect = 'za.co.absa.novofx.beta:id/ll_choose_app_country'
    editText = 'android.widget.EditText'
    SignInCredentials = ''
    SA_Password = "Password123!"
    SelectedCountry = 'za.co.absa.novofx.beta:id/iv_country_flag'
    btnSelectBeneficiary = 'za.co.absa.novofx.beta:id/bt_progress_button'
    btnContinue = 'za.co.absa.novofx.beta:id/bt_progress_button'
    email = 'za.co.absa.novofx.beta:id/et_user_input'
    username = 'za.co.absa.novofx.beta:id/et_user_input'
    Password = 'za.co.absa.novofx.beta:id/vet_login_password'
    btnLogin = 'za.co.absa.novofx.beta:id/bt_progress_button'
    btnGotIt_Security = 'za.co.absa.novofx.beta:id/bt_external_app_got_it'


class HomeScreen:
    sourceCurrency = 'za.co.absa.novofx.beta:id/et_home_source_currency_amount'
    targetCurrency = '	za.co.absa.novofx.beta:id/et_home_target_currency_amount'
    selectCurrency = 'za.co.absa.novofx.beta:id/iv_home_target_currency_flag'
    btnSelectBeneficiary = "//*[contains(@resource-id,'za.co.absa.novofx.beta:id/bt_progress_button')]"
    trxAmt = '504'

# Asserts
    firstName = 'za.co.absa.novofx.beta:id/tv_home_full_name'  # add asserts to this
    NovoLogo = 'za.co.absa.novofx.beta:id/tv_title'  # NovoFx logo
    transactionMaxLimit = 'za.co.absa.novofx.beta:id/tv_home_rand_limit_error' #1,000.000
    transactionMinLimit = 'za.co.absa.novofx.beta:id/tv_home_rand_limit_error' #500


class Beneficiary:
    BenTimmy = 'za.co.absa.novofx.beta:id/tv_initials'
    Search = 'za.co.absa.novofx.beta:id/tv_search'
    selectedBeneficiary = 'za.co.absa.novofx.beta:id/tv_select_beneficiary_name'
    Name = 'Bxjbx'
    btnNext = 'za.co.absa.novofx.beta:id/bt_progress_button'


class PaymentReason:
    Gift = "//*[@text ='Gift Payment']"
    Investment = "//*[@text ='Investment Payment']"
    Travel = "//*[@text ='Travel Payment']"
    btnNext = 'za.co.absa.novofx.beta:id/bt_progress_button'


class AdditionalInfo:
    SDA_Acknowledgement = "//*[contains(@text,'SDA Acknowledgement')]/ancestor::*[position()=4]/descendant::*[@resource-id='za.co.absa.novofx.beta:id/cb_question_boolean']"
    GiftToSelf = "//*[contains(@text,'Gift To Self Acknowledgement')]/ancestor::*[position()=4]/descendant::*[@resource-id='za.co.absa.novofx.beta:id/cb_question_boolean']"
    btnNext = 'za.co.absa.novofx.beta:id/bt_progress_button'

class Account:
    accountRadioBtn = 'za.co.absa.novofx.beta:id/rb_account_select'
    AmountLbl = 'za.co.absa.novofx.beta:id/tv_updating_exchange_rate'
    btnNext = 'za.co.absa.novofx.beta:id/bt_progress_button'

class Declaration:
    TermsLbl = "//*[@text ='I have read the Terms and conditions and know and understand the contents thereof;']"
    Accept = 'za.co.absa.novofx.beta:id/cb_declaration_accept'
    btnDeclarationContinue = 'za.co.absa.novofx.beta:id/bt_declaration_continue'
    popUpChkBox = 'za.co.absa.novofx.beta:id/cb_dialog_exchange_rate_changes_noshow'
    btnPopUp = 'za.co.absa.novofx.beta:id/bt_dialog_exchange_rate_changes_next'
    btnNewDealRate = 'za.co.absa.novofx.beta:id/bt_dialog_exchange_rate_changes_next'


