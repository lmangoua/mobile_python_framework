

class AppLoad:
    # click Proceed button
    #btnContinue = '//XCUIElementTypeButton[@name="Continue"]'
    btnContinue = "Continue"


class Login:
    #CountrySelect = 'create_beneficiary_bt_select_country'
    SignInCredentials = 'novoretail1@novo.com'
    SA_Password = 'Password123!'
    #btnCountrySelect = '//XCUIElementTypeStaticText[@name="Select country"]'
    btnCountrySelect = 'create_beneficiary_bt_select_country'
    #btnCountrySelect = 'create_beneficiary_bt_select_country'
    editCountry = 'Search'
    tapCountry = 'countryName'
    btnOkay = 'Okay'
    username = "//*[contains(@label, 'Email address')]/ancestor::*[position()=1]/descendant::*[@type = 'XCUIElementTypeTextField']"
    Password = "//*[contains(@label, 'Password')]/ancestor::*[position()=1]/descendant::*[@type = 'XCUIElementTypeSecureTextField']"
    btnLogin = 'signin_bt_signin'
    btnGotIt_Security = 'Got it!'
    btnSignUp = 'signin_bt_signup'
    chkBox = 'ic pink checkbox empty'
    btnContinue = 'Continue'


class Keyboard_iOS:
    btnKeyboardDone = 'Toolbar Done Button'
    btnKeyboard123 = 'more'
    btnKeyboardNext = 'Next:'
    btnKeyboardDelete = 'delete'
    btnKeybboardSearch = "//XCUIElementTypeButton[@name='Search']"


class HomeScreen:
    btnGotIt_Security = 'Got it!'
    sourceCurrency = 'home_tf_source_amount'
    targetCurrency = 'home_tf_target_amount'
    selectCurrency = 'za.co.absa.novofx.beta:id/iv_home_target_currency_flag'
    btnSelectBeneficiary = 'home_bt_select_ben'
    trxAmt = '504'

# Asserts
    firstName = 'usernameLabel'  # add asserts to this
    NovoLogo = 'za.co.absa.novofx.beta:id/tv_title'  # NovoFx logo
    transactionMaxLimit = 'za.co.absa.novofx.beta:id/tv_home_rand_limit_error' #1,000.000
    transactionMinLimit = 'za.co.absa.novofx.beta:id/tv_home_rand_limit_error' #500
    rate = 'rateLabel'


class Beneficiary:
    BenTimmy = 'TT'
    Search = 'Search'
    Name = 'Timmy'
    btnNext = 'Next'


class PaymentReason:
    Gift = "//*[@text ='Gift Payment']"
    Investment = "//*[@text ='Investment Payment']"
    Travel = "//*[@text ='Travel Payment']"
    btnNext = 'za.co.absa.novofx.beta:id/bt_progress_button'


class AdditionalInfo:
    SDA_Acknowledgement = "//*[contains(@text,'SDA Acknowledgement')]/ancestor::*[position()=4]/descendant::*[@resource-id='za.co.absa.novofx.beta:id/cb_question_boolean']"
    GiftToSelf = "//*[contains(@text,'Gift To Self Acknowledgement')]/ancestor::*[position()=4]/descendant::*[@resource-id='za.co.absa.novofx.beta:id/cb_question_boolean']"
    btnNext = 'za.co.absa.novofx.beta:id/bt_progress_button'

class Account:
    accountRadioBtn = 'za.co.absa.novofx.beta:id/rb_account_select'
    AmountLbl = 'za.co.absa.novofx.beta:id/tv_updating_exchange_rate'
    btnNext = 'za.co.absa.novofx.beta:id/bt_progress_button'

class Declaration:
    TermsLbl = "//*[@text ='I have read the Terms and conditions and know and understand the contents thereof;']"
    Accept = 'za.co.absa.novofx.beta:id/cb_declaration_accept'
    btnDeclarationContinue = 'za.co.absa.novofx.beta:id/bt_declaration_continue'
    popUpChkBox = 'za.co.absa.novofx.beta:id/cb_dialog_exchange_rate_changes_noshow'
    btnPopUp = 'za.co.absa.novofx.beta:id/bt_dialog_exchange_rate_changes_next'
    btnNewDealRate = 'za.co.absa.novofx.beta:id/bt_dialog_exchange_rate_changes_next'

