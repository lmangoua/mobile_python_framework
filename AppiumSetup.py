from appium import webdriver


class AppiumSetup:

    desired_cap = {
        "deviceName": "Huawei P10",
        "udid": "7EX0218118002238",
        "platformName": "Android",
        "platformVersion": "8.0",
        "appPackage": "za.co.absa.novofx.beta",
        "appActivity": "za.co.absa.novofx.ui.activities.LaunchActivity"
    }

    desired_cap_Samsung = {
        "deviceName": "Samsung G570F",
        "udid": "32013716427615ff",
        "platformName": "Android",
        "platformVersion": "7.0",
        "appPackage": "za.co.absa.novofx.beta",
        "appActivity": "za.co.absa.novofx.ui.activities.LaunchActivity"
    }

    desired_cap_Elephone = {
        "deviceName": "Elephone P8_Mini",
        "udid": "9TN76PPZBQUWZ9I7",
        "platformName": "Android",
        "platformVersion": "7.1",
        "appPackage": "za.co.absa.novofx.beta",
        "appActivity": "za.co.absa.novofx.ui.activities.LaunchActivity"
    }

    desired_cap_Umidigi = {
            "deviceName": "A-gold F1",
            "udid": "F120190100010350",
            "platformName": "Android",
            "platformVersion": "8.1",
            "appPackage": "za.co.absa.novofx.beta",
            "appActivity": "za.co.absa.novofx.ui.activities.LaunchActivity"
    }

    desired_cap_iPhoneSE = {
        "app": "/Users/jeff/Library/Developer/Xcode/DerivedData/NovoFx-gxnxmrkthmvmweadypnbqwcceysd/Build/Products/Debug-iphonesimulator/beta.app",
        "autoGrantPermissions": 'true',
        "automationName": "XCUITest",
        "deviceName": "iPhone SE",
        "udid": "C7015D73-FF74-43BF-B178-C76F096F913E",
        "platformName": "IOS",
        "platformVersion": "13.1"
    }

    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap_iPhoneSE)
    driver_iOS_SE = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap_iPhoneSE)
#    driverSamsung = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap_Samsung)

