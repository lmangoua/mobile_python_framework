from Elements import iOSElements
from appium.webdriver.common.touch_action import TouchAction

from AppiumSetup import AppiumSetup
from selenium.webdriver.support.ui import WebDriverWait
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support import expected_conditions as EC
import subprocess



driver = AppiumSetup.driver_iOS_SE
driver.set_page_load_timeout(20000)



AppLoad = iOSElements.AppLoad
Login = iOSElements.Login
Home = iOSElements.HomeScreen
Beneficiary = iOSElements.Beneficiary
PaymentReason = iOSElements.PaymentReason
AdditionalInfo = iOSElements.AdditionalInfo
Account = iOSElements.Account
Declaration = iOSElements.Declaration


#Appload

driver.implicitly_wait(25)
#driver.find_element_by_accessibility_id(AppLoad.btnContinue).click()


Continue = WebDriverWait(driver, 30).until(
    EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, AppLoad.btnContinue))
)
Continue.click()

CountrySelect = WebDriverWait(driver, 30).until(
    EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Login.btnCountrySelect))
)
CountrySelect.click()

editCountryField = WebDriverWait(driver, 30).until(
    EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Login.editCountry))
)
editCountryField.send_keys("South Africa")

tapCountry = WebDriverWait(driver, 30).until(
    EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Login.tapCountry))
)
tapCountry.click()

Continue = WebDriverWait(driver, 30).until(
    EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, AppLoad.btnContinue))
)
Continue.click()

# Login
editCountryField = WebDriverWait(driver, 30).until(
    EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Login.editCountry))
)
editCountryField.send_keys("South Africa")


#
driver.quit()
