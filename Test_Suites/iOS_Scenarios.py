import unittest
from appium import webdriver

from Elements import iOSElements

from AppiumSetup import AppiumSetup
from selenium.webdriver.support.ui import WebDriverWait
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support import expected_conditions as EC
import subprocess


driver = AppiumSetup.driver_iOS_SE

Keyboard = iOSElements.Keyboard_iOS
AppLoad = iOSElements.AppLoad
Login = iOSElements.Login
Homescreen = iOSElements.HomeScreen
Beneficiary = iOSElements.Beneficiary
PaymentReason = iOSElements.PaymentReason
AdditionalInfo = iOSElements.AdditionalInfo
Account = iOSElements.Account
Declaration = iOSElements.Declaration

class AppiumTest(unittest.TestCase):


    def setUp(self):
        driver = AppiumSetup.driver_iOS_SE
        driver.set_page_load_timeout(20000)


    def test_AppLoad():
        driver.implicitly_wait(25)
        # driver.find_element_by_accessibility_id(AppLoad.btnContinue).click()

        Continue = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, AppLoad.btnContinue))
        )
        Continue.click()

        CountrySelect = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Login.btnCountrySelect))
        )
        CountrySelect.click()

        editCountryField = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Login.editCountry))
        )
        editCountryField.send_keys("South Africa")

        tapCountry = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Login.tapCountry))
        )
        tapCountry.click()

        Continue = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, AppLoad.btnContinue))
        )
        Continue.click()
        driver.implicitly_wait(20)

    @staticmethod
    def test_Login():
        driver.implicitly_wait(20)
        editEmail = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.XPATH, Login.username))
        )
        editEmail.send_keys(Login.SignInCredentials)
        #

        editPassword = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.XPATH, Login.Password))
        )
        editPassword.send_keys(Login.SA_Password)

        driver.implicitly_wait(20)

        KbDone = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Keyboard.btnKeyboardDone))
        )
        KbDone.click()

        signIn = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Login.btnLogin))
        )
        signIn.click()

    def test_HomeScreen():
        driver.implicitly_wait(20)

        securityImportant = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Homescreen.btnGotIt_Security))
        )
        securityImportant.click()

        sourceAmt = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Homescreen.sourceCurrency))
        )
        sourceAmt.send_keys(Homescreen.trxAmt)

        KbDone = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Keyboard.btnKeyboardDone))
        )
        KbDone.click()

        selectBeneficiary = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Homescreen.btnSelectBeneficiary))
        )
        selectBeneficiary.click()

    def test_Beneficiary():
        driver.implicitly_wait(20)

        searchField = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Beneficiary.Search))
        )
        searchField.click()

        driver.find_element_by_accessibility_id(Beneficiary.Search).send_keys(Beneficiary.Name)

        #here you can either click on name or initials as accessibility ID
        selectBeneficiary = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Beneficiary.BenTimmy))
        )
        selectBeneficiary.click()

        nextButton = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((MobileBy.ACCESSIBILITY_ID, Beneficiary.btnNext))
        )
        nextButton.click()

    def tearDown(self):
        driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(AppiumTest())
    unittest.TestSuite.run(suite)
